# AvyReach avalanche Report

This repository contains the Latex files which are used to create the forms to decode the shortened avalanche bulletin, that is send to the InReach device from the [AvyReach](https://codeberg.org/fAiLix/AvyReach) application.

## overview

This file includes the description of the avalanche format and how to interpret the values


## bulletin-form

This file includes the form where one can write down the decoded values from the avalanche report.